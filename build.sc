import mill._, scalalib._, publish._

final object Global {
  @inline final val dottyVersion = "0.21.0-bin-20191113-87ea7a6-NIGHTLY"
}

trait Playground extends ScalaModule {

  @inline final def scalaVersion = Global.dottyVersion

  @inline final def scalacOptions = Seq(
    // "-P:acyclic:force",
    "-encoding", "UTF-8",
    "-deprecation",
    // "-Xprint:all",
    "-unchecked",
    "-feature",
    "-target:jvm-1.8",
    "-migration",
    "-Xfatal-warnings",
    // "-opt:l:inline", "-opt-inline-from:**",
    "-language:higherKinds",
    // "-opt:box-unbox", "-opt:nullness-tracking"
  )
}

final object play extends Playground {

  @inline final def millSourcePath =
    os.pwd / "modules" / "play"

  @inline final def publishVersion = "0.0.1"
}

// final object laws extends Playground {
//   @inline final def millSourcePath =
//     os.pwd / "modules" / "laws"

//   @inline final def publishVersion = "0.0.1"
// }
