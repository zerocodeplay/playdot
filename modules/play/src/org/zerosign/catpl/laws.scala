package org.zerosign.play.laws


object monoid {
  import org.zerosign.play.types.{ Eql, Monoid }

  inline def assoc[A](x: A, y: A, z: A)(given Monoid[A], Eql[A]) : Boolean =
    x.combine(y).combine(z) == x.combine(y.combine(z))

  inline def identity[A](x: A)(given inline m : Monoid[A], inline eqv: Eql[A]) : Boolean =
    x.combine(m.empty) == x && m.empty.combine(x) == x
}
