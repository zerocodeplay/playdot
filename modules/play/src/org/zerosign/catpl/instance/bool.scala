package org.zerosign.play.instance.bool

import org.zerosign.play.types.{ Monoid, Semigroup }

object ops {

  given OrBool : Semigroup[Boolean] {
    def (x: Boolean) combine (y: Boolean) : Boolean =
      x || y
  }

  given AndBool : Semigroup[Boolean] {
    def (x: Boolean) combine (y: Boolean) : Boolean =
      x && y
  }
}

given (given Semigroup[Boolean]) : Monoid[Boolean] {
  def (x: Boolean) combine (y: Boolean) : Boolean = x.combine(y)
  def empty: Boolean = false
}
