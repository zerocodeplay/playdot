package org.zerosign.play.instance.integer

import org.zerosign.play.types.{ Semigroup, Monoid }

object ops {
  import org.zerosign.play.types.{ Eql, Show, Ord, Ordering }

  given PlusInt : Semigroup[Int] {
    def (x: Int) combine (y: Int) : Int =
      x + y
  }

  given Ord[Int] {
    def (x: Int) compare (y: Int) =
      if (x < y) Ordering.Lower else if (x > y) Ordering.Greater else Ordering.Equal
  }

  given Show[Int] {
    def (x: Int) show() : String =
      x.toString
  }

  given Eql[Int] {
    def (x: Int) equal (y: Int) = x == y
  }
}

given (given Semigroup[Int]) : Monoid[Int] {
  def (x: Int) combine (y: Int) : Int = x.combine(y)
  def empty : Int = 0
}
