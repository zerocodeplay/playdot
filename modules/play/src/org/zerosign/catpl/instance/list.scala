package org.zerosign.play.instance.list

object ops {
  import org.zerosign.play.types.{ Eql, Show, Ord, Ordering }

  given [T](given ord: Eql[T]) : Eql[List[T]] {
    def (xs: List[T]) equal (ys: List[T]) : Boolean = (xs, ys) match {
      case (Nil, Nil) => true
      case (Nil, _) => false
      case (_, Nil) => false
      case (x :: xl, y :: yl) =>
        x.equal(y) && xl.equal(yl)
    }
  }

  given [T](given show: Show[T]) : Show[List[T]] {
    def (xs: List[T]) show() : String = xs.mkString("[", ",", "]")
  }

  given [T](given ord: Ord[T]) : Ord[List[T]] {
    def (xs: List[T]) compare (ys: List[T]) : Ordering = (xs, ys) match {
      case (Nil, Nil) => Ordering.Equal
      case (Nil, _) => Ordering.Lower
      case (_, Nil) => Ordering.Greater
      case (x :: xl, y :: yl) =>
        x.compare(y) match {
          case Ordering.Equal => Ordering.Equal
          case _ => xs.compare(yl)
        }
    }
  }
}
