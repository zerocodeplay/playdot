package org.zerosign.play.option

enum Option[+T] {
  case Some(v: T) extends Option[T]
  case None extends Option[Nothing]

  inline def isDefined : Boolean = this match {
    case Option.None => false
    case _ => true
  }
}

object Option {
  inline def apply[T](x: T) : Option[T] =
    if (x == null) Option.None else Option.Some(x)
}

object ops {
  import org.zerosign.play.types.{ FlatMap, Applicative, Functor, Monad, Monoid }

  given FlatMap[Option] {
    def [A, B](value: Option[A]) flatMap (f: A => Option[B]) : Option[B] =
      value match {
        case Option.Some(v: A) => f(v)
        case _ => Option.None
      }
  }

  given Applicative[Option] {
    def pure[A](value: A) : Option[A] = Option.apply(value)
  }

  given [A](given Functor[Option], Applicative[Option], FlatMap[Option]) : Monad[Option] {
    def pure[A](value: A) : Option[A] = Option.apply[A](value)

    def [A, B](value: Option[A]) map (f: A => B) : Option[B] =
      value.map(f)

    def [A, B](value: Option[A]) flatMap (f: A => Option[B]) : Option[B] =
      value.flatMap(f)
  }

  given [A](given Monoid[A]) : Monoid[Option[A]] {
    def (x: Option[A]) combine (y: Option[A]) : Option[A] =
      (x, y) match {
        case (Option.Some(x), Option.Some(y)) => Option.Some(x.combine(y))
        case _ => Option.None
      }

    def empty : Option[A] = Option.None
  }

  given [B] : Functor[Option] {
    def [A, B](fa: Option[A]) map (f: A => B) : Option[B] =
      fa match {
        case Option.Some(a) => Option.Some(f(a))
        case _ => Option.None
      }
  }
}
