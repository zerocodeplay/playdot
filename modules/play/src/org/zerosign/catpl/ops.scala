package org.zerosign.play.ops

import org.zerosign.play.types.{Show, Ord, Ordering}

inline def show[T](el: T)(given Show[T]) : String =
  el.show()

inline def maximum[T](xs: List[T])(given Ord[T]) : T =
  xs.reduceLeft { case (x, y) => max(x, y) }

inline def max[T](x: T, y: T)(given Ord[T]) : T =
  x.compare(y) match {
    case Ordering.Lower => y
    case _ => x
  }
