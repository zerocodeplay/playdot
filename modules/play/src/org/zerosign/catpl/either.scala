package org.zerosign.play.either

import org.zerosign.play.option
import option._
import option.ops._

enum Either[A, B] {
  case Left(v: A) extends Either[A, B]
  case Right(v: B) extends Either[A, B]

  inline def isLeft : Boolean = this match {
    case Either.Left(_) => true
    case _ => false
  }

  inline def isRight : Boolean = this match {
    case Either.Right(_) => true
    case _ => false
  }

  inline def toRight : Option[B] = this match {
    case Either.Right(v: B) => Option.Some(v)
    case _ => Option.None
  }

  inline def toLeft : Option[A] = this match {
    case Either.Left(v: A) => Option.Some(v)
    case _ => Option.None
  }
}

object Either {
  inline def left[A, B](v: A) : Either[A, B] = Either.Left(v)
  inline def right[A, B](v: B) : Either[A, B] = Either.Right(v)
}

object ops {
  import org.zerosign.play.types.BiFunctor

  given BiFunctor[Either] {
    def [A, B, C, D](fab: Either[A, B]) bimap (f: A => C, g: B => D) : Either[C, D] =
      fab match {
        case Either.Left(v) => Either.left(f(v))
        case Either.Right(v) => Either.right(g(v))
      }
  }
}
