package org.zerosign.play.types

type Id[A] = A

trait FlatMap[F[_]] {
  def [A, B](value: F[A]) flatMap (f: A => F[B]) : F[B]
}

trait Applicative[F[_]] {
  def pure[A](value: A) : F[A]
}

trait BiFunctor[F[A, B]] {
  def [A, B, C, D](fab: F[A, B]) bimap (f: A => C, g: B => D) : F[C, D]
}

trait Monad[F[_]] extends Functor[F] with FlatMap[F] with Applicative[F]

trait Functor[F[_]] {
  def [A, B] (fa: F[A]) map (f: A => B) : F[B]
}

trait Semigroup[A] {
  def (x: A) combine (y: A) : A
}

trait Monoid[A] extends Semigroup[A] {
  def empty: A
}

trait Eql[T] {
  def (x: T) equal (y: T) : Boolean
  inline def (x: T) == (y: T) = x.equal(y)
}

enum Ordering(x: Int) {
  case Lower extends Ordering(-1)
  case Equal extends Ordering(0)
  case Greater extends Ordering(1)

  override final val ordinal : Int = x
}

trait Ord[T] {
  def (x: T) compare (y: T) : Ordering
  inline def (x: T) < (y: T) = x.compare(y) == Ordering.Lower
  inline def (x: T) > (y: T) = x.compare(y) == Ordering.Greater
}

trait Show[T] {
  def (el: T) show() : String
}
